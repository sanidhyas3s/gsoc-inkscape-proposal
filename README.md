# <img src="assets/inkscape.png" height=30> **INKSCAPE GSoC PROPOSAL**

## **About Me**
Hi! I am **Sanidhya Singh**, and I am a sophomore undergraduate student in Computer Science and Engineering at Indian Institute of Technology Roorkee (IIT Roorkee), India.

### **Personal Details**
- Mail: sanidhyas3s@gmail.com & s_singh1@cs.iitr.ac.in 
- GitLab: [sanidhyas3s](https://gitlab.com/sanidhyas3s) & GitHub: [sanidhyas3s](https://github.com/sanidhyas3s)
- Inkscape Rocket Chat: @sanidhyas3s
- Location and Timezone: Roorkee, India, IST (UTC+5:30)

Yeah, basically you can find me at almost all places with "@sanidhyas3s"

### **Academic Details**
- **Institute**: Indian Institute of Technology Roorkee (IIT Roorkee)
- **Degree**: Computer Science and Engineering (B.Tech, Expected Graduation - 2025)
- I have undertaken pertinent **coursework** during my degree program, including Object Oriented Programming & Design, Software Engineering, Data Structures & Algorithms, Operating Systems, Computer Architecture.
  
### **Programming Skills and Tools**

- I have a strong command over **C++** and **Python**, and also have a good grasp of HTML, CSS, and JavaScript, including various frameworks.
- Additionally, I have experience working with C, Java, and Verilog as well.
-  I am equally comfortable working on both **Ubuntu** and **Windows**, although my current Inkscape development setup is exclusively on Ubuntu. 
-  For version control, I have extensively used **Git**, which I have experience with on both GitLab and GitHub. 
-  I use **Visual Studio Code** as my primary code editor, and I have personalised it extensively with various extensions to enhance my workflow.
- Unlike most others, I rather enjoy creating documentation and I am well-versed with tools such as Markdown and LaTeX.

Besides development, I also have a deep interest in **Machine Learning**, and actively participate in various hackathons and ML/AI challenges as a member of the [Data Science Group, IIT Roorkee](https://github.com/dsgiitr).

Anything related to technology generally sparks my interest and am actively looking at the latest releases of software, devices and more. 

### **My Journey with Inkscape**
A few years ago, I discovered Inkscape as a free alternative to other expensive vector design software. If it wasn't for Inkscape, I might have given up on trying out designing because of not having any free accessible tool. 

It was actually through Inkscape that I learnt about open-source software and how anyone capable can contribute to such projects and this mildly fascinated me. Since then, I have been eager to contribute to Inkscape as a way of giving back to the community.

I have used Inkscape to make a lot of illustrations and graphics (posters mainly) sometimes as a hobby to free my mind and for work, the rest of the time.

The following is by far my most extensive work in Inkscape, finished over the span of a few weeks. It is actually a digital-recreation of an oil-pastel painting I made.
<figure>
    <img src="assets/dusk.png" width=400>
    <figcaption><i>Reflections at Dusk</i></figcaption>
</figure>

I frequently participate in [r/Inkscape](https://www.reddit.com/r/Inkscape), a vibrant community on Reddit dedicated to discussing Inkscape. Being a part of this community has been a source of great inspiration for me, as I explore new ways to use the various tools available in Inkscape.


### **Contributions to Inkscape**

Starting from last year, I have tried to contribute to Inkscape and have browsed through a lot of the code-base while attempting to fix bugs or make improvements.

Formally listing, I have made the following 6 Merge Requests to the Inkscape repository so far:
- [!5229](https://gitlab.com/inkscape/inkscape/-/merge_requests/5229) (Merged)
- [!5166](https://gitlab.com/inkscape/inkscape/-/merge_requests/5166) (Merged)
- [!5135](https://gitlab.com/inkscape/inkscape/-/merge_requests/5135) (Merged)
- [!5232](https://gitlab.com/inkscape/inkscape/-/merge_requests/5232) (Approved)*
- [!5169](https://gitlab.com/inkscape/inkscape/-/merge_requests/5169) (Approved)*
- [!5157](https://gitlab.com/inkscape/inkscape/-/merge_requests/5157) (Open)*
- [!5080](https://gitlab.com/inkscape/inkscape/-/merge_requests/5080)  (Open)*
- [!4778](https://gitlab.com/inkscape/inkscape/-/merge_requests/4778) (Closed)


Along with coding, I have involved myself with discussions about applicability and reproducibility of issues. Also, I have reported the following issue myself:
- [#8364](https://gitlab.com/inkscape/inbox/-/issues/8364) (in Inbox)
- [#8426](https://gitlab.com/inkscape/inbox/-/issues/8426) (Closed by !5229 in Inbox)

<sup> \* Status as of 2 June. </sup>

---

## **The Project**
### *Customizable Appearance of Canvas Controls*

Canvas controls are arguably the most essential component of Inkscape, they are present literally everywhere, from nodes to handles of gradients and more. How they visually appear plays a quite important part in the user experience while working in Inkscape.

<figure>
    <img src="assets/current.png" width=480>
    <figcaption><i>The current appearance of node handles</i></figcaption>
</figure>

They are currently very unappealing and haven't really evolved in design with time. The styling of these controls is hard-coded presently and offers no direct customization options to the users.

Changing the appearance to some other default look will only ever be a stop-gap solution to questions about what the best option is, considering functionality to the user; while maintaining an acceptable visual appearance, and there would never be 100% agreement on any particular design. [This discussion thread](https://gitlab.com/inkscape/ux/-/issues/115) is a clear evidence of that.

<figure>
    <img src="assets/proposed.png" width=700>
    <figcaption><i>Proposed design for node handles (by Adam Belis)</i></figcaption>
</figure>

So, as a flexible solution to the above stated problem, this project will provide the ability to change the appearance of the controls through just a CSS file making it possible for everyone to customize it according to their liking. This also makes it easier to change future default styling appearance of the controls.

### **Technical Overview and Examples**

The planned way of integration is that a default CSS specifying the styles of the various controls would be shipped with every Inkscape installation. The user can then further provide their own customized CSS file according to their requirements which will override the default one. The technical implementation would include parsing/reading the CSS file through **`libcroco`**, and then the rendering would be done using **`cairo`** taking the styling properties applied into account, through a simplistic renderer which can be extended in use-case rather easily unlike the existing implementation of rendering that can be found in the `build_cache()` and `_render()` functions found in `src/display/control/canvas-item-ctrl.cpp`.

An example snippet of how the CSS can look:
```css
    .inkscape-node-smooth {
        -inkscape-node-shape: "square";
        outline-width: 2px;
        outline-color: #747474;
        border-width: 4px;
        border-color: blue;
        fill: white;
        ...
    }

    .inkscape-node-smooth:hover {
        fill: orange;
    }
```
These CSS files could be easily modified and read. 
The proposed mechanism will provide support for a limited set of CSS properties that are appropriate for styling the controls of the canvas. This functionality will include the ability to style various shapes of nodes, as well as CSS properties such as fill, border-color, border-width, outline-color, outline-width, and pseudo-classes like :hover.

<figure>
    <img src="assets/mockups.png" width=700>
    <figcaption><i>Some of the different variations of node handle styles possible</i></figcaption>
</figure>

The above sample demonstrates different possible styles that could be applied to the control handles; like different fill-color, border-color, changing fill-color on hover, increasing node size on hover, different outline width, customizable node shapes and more.

In brief, the project aims to achieve the following:
- Ability to modify the appearance of controls through **CSS properties** and hence enable customization for users.
- Have a **modular implementation** for the rendering of the control handles. Basically, improving the quality of concerned parts of code.
- Introduce **outlines** for the controls making it easier for them to be separated from the canvas in combinations with less contrast.

Eventually, all handles/controls can be migrated to the implemented renderer, but the Node Tool will be used as a starting point and further integration can include other tools like the Gradient Tool.
If enough time is available, additionally there could be a specific preferences page allocated to these customizations to further ease the access for the user.
Moreover, additional CSS properties (such as mix-blend-mode) and more node shapes could also be added.

This project will open up a lot of opportunities to further enhance the performance of control handles and their functionality by providing a good structured implementation of rendering of control handles and simple ways to modify their styles. Additionally, this project could potentially serve as a stepping stone towards developing theming and styling options for other UI elements in Inkscape.


### **Tentative Timeline**

The timeline is highly tentative and may very well change, depending on the progress, increased deliverables and time taken in completing various aspects of the project.

- **Week 1-2**: Extensive planning and discussion regarding the **design** of the classes and their functioning keeping compatibility and upgradability in context also.
- **Week 3-5**: Implementing the design in **code** starting from parsing the CSS and then moving on to the rendering process.
- **Week 6-8**: Extensive **testing** and **fixing** any bugs found in the process based on the feedback.
- **Week 9-12**: Working on further integration and attempting to implement optional features.

Community feedback, especially from the UX team would be highly valued throughout the span of the project. Secondly, I would try to ensure that whatever code is written is accompanied with quality documentation,  it is easy to modify and extend from in the future if needed.

---

Thank You for Reading!

**Updates**:

- This Project Proposal was accepted into Google Summer of Code!

- Blogs reporting Project Progress can be found in [`./blogs/`](https://gitlab.com/sanidhyas3s/gsoc-inkscape-proposal/-/tree/main/blogs).

---